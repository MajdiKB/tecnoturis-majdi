# Welcome to my Tecnoturis challenge!

I use TMDB API (Movies API) to get the info.

The front was developed with  Angular (12.2.11). (with Bootstrap for de Cards & Buttons)

Finally, I used the library ***ngx-cookie-service*** to control de user Token (reqres API).

## Steps to use the app.

### Clone the repository.

*Clone the repository in local folder in your system.*

`git@gitlab.com:MajdiKB/tecnoturis-majdi.git`

## Install the dependencies.

*In the folder where you cloned the repository, execute:*

`npm i`

## Run the app.

*Finally run the app.*

`npm start`

## IMPORTANT!

To use the app, you can login o register with this user example:

`user: george.edwards@reqres.in`

`pass: any`
***
*Majdi Kokaly.*
[LinkedIn](https://www.linkedin.com/in/majdi-kokaly/)