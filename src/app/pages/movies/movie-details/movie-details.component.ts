import { UserService } from './../../../shared/services/user.service';
import { Location } from '@angular/common';
import { MovieModelFront } from 'src/app/models/movie-model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesApiService } from 'src/app/shared/services/service-api.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  public movieId: string = '';
  public movieDetails: any = {};
  public userToken: string = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private moviesApiService: MoviesApiService,
    private userService: UserService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.movieId = params.get('id') as string;
      this.getMovieDetail(this.movieId);
      window.scroll(0, 0);
    });
    this.userToken = this.userService.getToken();
  }

  public getMovieDetail(movieId: string): void {
    this.moviesApiService
      .getMovieDetail(movieId)
      .subscribe((data: MovieModelFront) => {
        this.movieDetails = data;
      });
  }

  public handleClickBack() {
    this.location.back();
  }
  public handleClickLogOut(): void {
    this.userService.destroyToken();
    this.router.navigateByUrl('/user/login');
    window.scroll(0, 0);
  }
}
