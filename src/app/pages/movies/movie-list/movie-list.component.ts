import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieModelFront } from 'src/app/models/movie-model';
import { MoviesApiService } from 'src/app/shared/services/service-api.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  public movieList: MovieModelFront[] = [];
  public movieOriginList: MovieModelFront[] = [];
  public searchValue: string = '';
  public matchSearch: boolean = false;
  public noresultList: string = "";
  public searchResult: MovieModelFront[] = [];
  public filterMovieList: MovieModelFront[] = [];
  public userToken: string = "";

  constructor(private moviesApiService: MoviesApiService, private router: Router, private location: Location, private userService: UserService) {}

  ngOnInit(): void {
    this.userToken = this.userService.getToken();
    if (this.userToken == ""){
      this.router.navigateByUrl('/user/login');
    }
    else{
    this.getmoviesList();
    window.scroll(0, 0);
  }
  }

  public getmoviesList(): void {
    this.moviesApiService.getMoviesList().subscribe((data: MovieModelFront[]) => {
      const result: MovieModelFront[] = data;
      this.movieList = result;
      this.movieOriginList = result;
    });
  }

  public handleSerchByName(event: any): MovieModelFront[] {
    console.log(event);
    console.log(this.movieList);
    console.log(this.movieOriginList);
    this.matchSearch = false;
    this.searchValue = event.target.value.toLowerCase().trim();
    let searchResult = [];
    if (!this.searchValue || this.searchValue == "") {
      this.movieList = this.movieOriginList;
      this.matchSearch = false;
    }
    searchResult = this.movieOriginList.filter(
      (item: MovieModelFront) => item.title.toLowerCase().indexOf(this.searchValue) > -1
    );
    if (searchResult.length > 0) {
      this.movieList = searchResult;
      this.noresultList = "";
    } else {
      this.movieList = [];
      this.noresultList = "Sorry, no movie with this title.";
    }
    return this.movieList;
  }

  public handleClickOrderByTitle(): void {
    this.movieList.sort((a: MovieModelFront, b: MovieModelFront) => {
      if (a.title < b.title) {
        return -1;
      }
      if (a.title > b.title) {
        return 1;
      }
      return 0;
    });
  }
  public handleClickOrderByPopularity(): void {
    this.movieList.sort((a: MovieModelFront, b: MovieModelFront) => {
      if (a.popularity > b.popularity) {
        return -1;
      }
      if (a.popularity < b.popularity) {
        return 1;
      }
      return 0;
    });
  }

  public handleClickLogOut(): void {
    this.userService.destroyToken();
    this.router.navigateByUrl('/user/login');
    window.scroll(0,0);
  }
}
