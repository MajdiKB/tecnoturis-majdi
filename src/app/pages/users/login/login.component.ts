import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public email: string = '';
  public password: string = '';
  public token: string = '';

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.token = this.userService.getToken();
    if (this.token != '') {
      this.router.navigateByUrl('/movies');
    }
  }

  public login() {
    const user = { email: this.email, password: this.password };
    this.userService.login(user).subscribe((data) => {
      this.userService.setToken(data.token);
      const token = this.userService.getToken();
      console.log(token);
      this.router.navigateByUrl('/movies');
    });
  }
}
