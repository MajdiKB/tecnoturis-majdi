import { UserService } from './../../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public email: string = '';
  public password: string = '';
  public token: string = '';

  constructor(private userService: UserService, private router: Router ) {}

  ngOnInit(): void {
    this.token = this.userService.getToken();
    if (this.token != '') {
      this.router.navigateByUrl('/movies');
    }
  }
  public register() {
    const user = { email: this.email, password: this.password };
    this.userService.register(user).subscribe(data => {
      this.userService.setToken(data.token);
      this.router.navigateByUrl('/movies');
    });
  }
}
