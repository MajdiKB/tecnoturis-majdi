import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CookieService } from "ngx-cookie-service";

@Injectable()
export class UserService {
  public apiLoginUserUrl : string = environment.apiLoginUserUrl;
  public apiRegisterUserUrl : string = environment.apiRegisterUserUrl;
  constructor(private http: HttpClient, private cookies: CookieService) { }

  public login(user: any): Observable<any> {
    return this.http.post(this.apiLoginUserUrl, user);
  }
  public register(user: any): Observable<any> {
    return this.http.post(this.apiRegisterUserUrl, user);
  }

  public setToken(token: string) {
    this.cookies.set("token", token);
  }
  public getToken() {
    return this.cookies.get("token");
  }
  public destroyToken() {
    return this.cookies.delete("token");
  }
}
