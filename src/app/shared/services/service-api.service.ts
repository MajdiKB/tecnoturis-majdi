import {
  HttpClient,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { MovieModelFront } from '../../models/movie-model';
@Injectable()
export class MoviesApiService {
  public baseUrl: string = environment.apiUrl;
  public apiKey: string = environment.apiKey;

  constructor(private http: HttpClient, private router: Router) {}

  public getMoviesList(): Observable<any> {
    return this.http.get(this.baseUrl + 'popular?api_key=' + this.apiKey).pipe(
      map((res: any) => {
        const results: any[] = res.results;
        const formattedResults: MovieModelFront[] = results.map(
          ({ id, title, popularity, poster_path }) => ({
            id,
            title,
            popularity,
            poster_path,
          })
        );
        return formattedResults;
      }),
      catchError((err) => {
        const error = 'No connection to DB';
        console.log(error);
        throw new Error(err.message);
      })
    );
  }
  public getMovieDetail(movieId: string): Observable<any> {
    return this.http
      .get(`${this.baseUrl}${movieId}?api_key=${this.apiKey}`)
      .pipe(
        map((res: any) => {
          const result: any = res;
          console.log(result);
          return result;
        }),
        catchError((err) => {
          const error = 'No connection to DB';
          console.log(error);
          throw new Error(err.message);
        })
      );
  }
}
