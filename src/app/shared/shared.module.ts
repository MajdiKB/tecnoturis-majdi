import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MoviesApiService } from './services/service-api.service';
import { FormsModule } from '@angular/forms';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, FormsModule],
  exports: [HttpClientModule, FormsModule],
  providers: [MoviesApiService, UserService],
})
export class SharedModule {}
