
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: `movies`, loadChildren: () =>
      import('./pages/movies/movie.module').then(m => m.MoviesModule)
  },
  {
    path: `user`, loadChildren: () =>
      import('./pages/users/users.module').then(m => m.UsersModule)
  },
  { path: ``, redirectTo: `user/login`, pathMatch: `full` },
  { path: `**`, redirectTo: `movies`, pathMatch: `full` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
