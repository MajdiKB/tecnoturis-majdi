export interface MovieModelFront {
  id: string;
  title: string;
  poster_path: string;
  popularity: number,
}
