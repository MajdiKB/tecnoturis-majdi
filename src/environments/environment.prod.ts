export const environment = {
  apiUrl: 'https://api.themoviedb.org/3/movie/',
  apiKey: '8f68c315d0d1a84d75d2524437901b75',
  apiRegisterUserUrl: 'https://reqres.in/api/register',
  apiLoginUserUrl: 'https://reqres.in/api/login',
  production: true
};
